const localizationMap = {
    SPRINTER: 'info.truck.types.sprinter',
    'SMALL STRAIGHT': 'info.truck.types.smallStraight',
    'LARGE STRAIGHT': 'info.truck.types.largeStraight',
    NEW: 'info.load.statuses.new',
    POSTED: 'info.load.statuses.posted',
    ASSIGNED: 'info.load.statuses.assigned',
    SHIPPED: 'info.load.statuses.shipped',
    'En route to Pick Up': 'info.load.states.routeToPickUp',
    'Arrived to Pick Up': 'info.load.states.arrivedToPickUp',
    'En route to delivery': 'info.load.states.routeToDelivery',
    'Arrived to delivery': 'info.load.states.arrivedToDelivery',
    OL: 'info.truck.statuses.ol',
    IS: 'info.truck.statuses.is',
    date: 'general.dateData',
};

export default localizationMap;
