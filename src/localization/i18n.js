import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import { format as formatDate } from 'date-fns';
import enUS from 'date-fns/locale/en-US';
import ru from 'date-fns/locale/ru';
import enLocale from './locales/en';
import uaLocale from './locales/ua';

const locales = {
    en: enUS, ua: ru,
};

function isDate(value) {
    if (value instanceof Date) {
        return true;
    }
    try {
        // eslint-disable-next-line no-unused-vars
        const date = new Date(value);
        return true;
    } catch (e) {
        return false;
    }
}

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        interpolation: {
            format(value, formatting, lng) {
                if (isDate(value)) {
                    const locale = locales[lng] || enLocale;
                    return formatDate(value, formatting, { locale });
                }
                return value.toString();
            },
            escapeValue: false,
        },

        fallbackLng: 'en',
        debug: true,

        resources: {
            en: enLocale, ua: uaLocale,
        },

        // have a common namespace used around the full app
        ns: ['translations'],
        defaultNS: 'translations',
    });

export default i18n;
