import React from 'react';
import { Route } from 'react-router-dom';
import HomePage from './components/pages/home_page/home_page';
import LoginPage from './components/pages/auth_pages/login_page';
import UserProfilePage from './components/pages/user_profile_page/user_profile';
import RegisterPage from './components/pages/auth_pages/register_page';
import LoadsPage from './components/pages/loads_pages/loads_page';
import LoadCreatorPage from './components/pages/loads_pages/load_creator_page';
import LoadsInfoPageWithFetch from './components/pages/loads_pages/load_info_page';
import LoadEditorPage from './components/pages/loads_pages/load_editor_page';
import AssignedLoadPage from './components/pages/loads_pages/assigned_load_page/assigned_load_page';
import AssignedTruckPage from './components/pages/trucks_pages/assigned_truck_page/assigned_truck_page';
import TrucksPage from './components/pages/trucks_pages/trucks_page';
import TruckCreatorPage from './components/pages/trucks_pages/truck_creator_page';
import TruckInfoPageWithFetch from './components/pages/trucks_pages/truck_info_page';
import TruckEditorPage from './components/pages/trucks_pages/truck_editor_page';
import ChatPage from './components/pages/chat_pages/chat_page/chat_page';
import ChooseChatPage from './components/pages/chat_pages/choose_chat_page/choose_chat_page';

export const ROUTES_WITH_COMPONENTS = {
    '/': HomePage,
    '/login': LoginPage,
    '/register': RegisterPage,
    '/profile': UserProfilePage,
    '/loads/create': LoadCreatorPage,
    '/loads/assigned': AssignedLoadPage,
    '/loads/:id/edit': LoadEditorPage,
    '/loads/:id': LoadsInfoPageWithFetch,
    '/loads': LoadsPage,
    '/trucks/assigned': AssignedTruckPage,
    '/trucks/create': TruckCreatorPage,
    '/trucks/:id/edit': TruckEditorPage,
    '/trucks/:id': TruckInfoPageWithFetch,
    '/trucks': TrucksPage,
    '/choose_chat': ChooseChatPage,
    '/chat': ChatPage,
};

export function getRoutes() {
    const routes = [];
    Object.entries(ROUTES_WITH_COMPONENTS).forEach(([path, component]) => {
        const route = (
            <Route
                key={path}
                exact={path === '/'}
                path={path}
                component={component}
            />
        );
        routes.push(route);
    });
    return routes;
}
