export function setLanguageToSessionStorage(language) {
    if (language) {
        window.sessionStorage.setItem('language', JSON.stringify(language));
    }
}

export function getLanguageFromSessionStorage() {
    return JSON.parse(window.sessionStorage.getItem('language'));
}

export function removeLanguageFromSessionStorage() {
    window.sessionStorage.removeItem('language');
}
