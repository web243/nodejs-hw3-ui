import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootSaga } from './saga/root_saga';
import { setUserToSessionStorage } from './user_session_storage';
import rootReducer from './reducers/root_reducer';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer, composeWithDevTools(
        applyMiddleware(sagaMiddleware),
    ),
);

sagaMiddleware.run(rootSaga);

window.addEventListener('unload', () => {
    const userState = store.getState().user;
    if (userState && userState.data) {
        setUserToSessionStorage(userState.data);
    }
});

export default store;
