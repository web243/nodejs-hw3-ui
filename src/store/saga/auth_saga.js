import { call, put, takeLatest } from 'redux-saga/effects';
import { setError, setUser } from '../reducers/user_reducer';
import { logInUser, signUpUser } from '../../services/auth_service';

function fetchUserByCredentials(email, password) {
    return logInUser({ email, password });
}

function postUserOnServer({ email, password, role }) {
    return signUpUser({ email, password, role });
}

function* getUserInfo(action) {
    try {
        const user = yield call(fetchUserByCredentials,
            action.payload.email,
            action.payload.password);
        yield put(setUser(user));
    } catch (e) {
        alert(e.message);
        yield put(setError({ message: e.message }));
    }
}

function* postUser(action) {
    try {
        yield call(postUserOnServer, action.payload);
        alert('Signed up successfully');
    } catch (e) {
        alert(e.message);
        yield put(setError({ message: e.message }));
    }
}

export function* logIn() {
    yield takeLatest('user/logIn', getUserInfo);
}

export function* register() {
    yield takeLatest('user/registerUser', postUser);
}
