import { all } from 'redux-saga/effects';
import { logIn, register } from './auth_saga';

// eslint-disable-next-line import/prefer-default-export
export function* rootSaga() {
    yield all([
        logIn(),
        register(),
    ]);
}
