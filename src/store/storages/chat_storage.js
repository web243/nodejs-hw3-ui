export function saveChatToStorage(chat) {
    if (chat) {
        window.localStorage.setItem('chat', JSON.stringify(chat));
    }
}

export function getChatFromStorage() {
    return JSON.parse(window.localStorage.getItem('chat'));
}

export function removeChatFromStorage() {
    window.localStorage.removeItem('chat');
}
