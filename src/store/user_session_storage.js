export function setUserToSessionStorage(user) {
    if (user) {
        window.sessionStorage.setItem('user', JSON.stringify(user));
    }
}

export function getUserFromSessionStorage() {
    return JSON.parse(window.sessionStorage.getItem('user'));
}

export function removeUserFromSessionStorage() {
    window.sessionStorage.removeItem('user');
}
