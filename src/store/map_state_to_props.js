const mapStateToProps = (state) => ({
    user: state.user.data,
    language: state.language.language,
    userError: state.user.error,
});

export default mapStateToProps;
