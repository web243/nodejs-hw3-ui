import { createSlice } from '@reduxjs/toolkit';
import {
    getUserFromSessionStorage,
    removeUserFromSessionStorage,
    setUserToSessionStorage,
} from '../user_session_storage';

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        data: getUserFromSessionStorage(),
    },
    reducers: {
        logIn: () => {},
        logOut: (state) => {
            state.data = null;
            removeUserFromSessionStorage();
        },
        registerUser: () => {},
        setUser: (state, action) => {
            const user = action.payload;
            const data = {
                id: user._id,
                email: user.email,
                role: user.role,
                token: user.token,
            };
            state.data = data;
            setUserToSessionStorage(data);
        },
        setError: (state, action) => {
            state.error = action.payload;
        },
    },
});

export const {
    logIn, logOut, setUser, setError, registerUser,
} = userSlice.actions;

export default userSlice.reducer;
