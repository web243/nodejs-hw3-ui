import { createSlice } from '@reduxjs/toolkit';
import {
    getLanguageFromSessionStorage,
    setLanguageToSessionStorage,
} from '../language_session_storage';

export const languageSlice = createSlice({
    name: 'language',
    initialState: {
        language: getLanguageFromSessionStorage(),
    },
    reducers: {
        setLanguage: (state, action) => {
            const language = action.payload;
            state.language = language;
            setLanguageToSessionStorage(language);
        },
    },
});

export const {
    setLanguage,
} = languageSlice.actions;

export default languageSlice.reducer;
