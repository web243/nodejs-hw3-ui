import { combineReducers } from 'redux';
import userReducer from './user_reducer';
import languageReducer from './language_reducer';

const rootReducer = combineReducers({
    user: userReducer,
    language: languageReducer,
});

export default rootReducer;
