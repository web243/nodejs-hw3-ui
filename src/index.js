import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/app';
import './index.css';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { QueryParamProvider } from 'use-query-params';
import { I18nextProvider } from 'react-i18next';
import store from './store/store';
import i18n from './localization/i18n';

function withStoreAndLocalization(WrappedComponent) {
    return (
        <Provider store={store}>
            <QueryParamProvider ReactRouterRoute={Route}>
                <I18nextProvider i18n={i18n}>
                    <WrappedComponent />
                </I18nextProvider>
            </QueryParamProvider>
        </Provider>
    );
}

ReactDOM.render(
    <BrowserRouter>
        {withStoreAndLocalization(App)}
    </BrowserRouter>,
    document.getElementById('root'),
);
