import {
    post, get, put, delete_, patch,
} from './fetch';
import authWrapper from './utils/auth_wrapper';

function getQuery(params) {
    return Object.entries(params).map(([key, value]) => (value ? `${key}=${value}` : '')).join('&');
}

function formLoad(data) {
    return {
        _id: data.id,
        name: data.name,
        pickup_address: data.pickupAddress,
        delivery_address: data.deliveryAddress,
        payload: data.payload,
        dimensions: {
            width: data.width,
            height: data.height,
            length: data.length,
        },
    };
}

export function getAllLoadsForUser(params) {
    let uri = '/loads';
    if (params) {
        const query = getQuery(params);
        uri = uri.concat(`?${query}`);
    }
    return authWrapper(get, uri)
        .then((result) => result.loads);
}

export function getLoadByIdForUser(loadId) {
    const uri = `/loads/${loadId}`;
    return authWrapper(get, uri);
}

export function getShippingInfoForUser(loadId) {
    const uri = `/loads/${loadId}/shipping_info`;
    return authWrapper(get, uri);
}

export function addLoadToUser(load) {
    const loadFormatted = formLoad(load);
    return authWrapper(post, '/loads', { body: loadFormatted });
}

export function updateLoadForUser(load) {
    const loadFormatted = formLoad(load);
    return authWrapper(put, `/loads/${loadFormatted._id}`, { body: loadFormatted });
}

export function postLoadByIdForUser(loadId) {
    return authWrapper(post, `/loads/${loadId}/post`)
        .then((body) => {
            if (body) {
                const driverFound = body.driver_found;
                const message = `Driver ${driverFound ? 'was' : 'was not'} found`;
                alert(message);
            }
        });
}

export function deleteLoadByIdForUser(loadId) {
    return authWrapper(delete_, `/loads/${loadId}`);
}

export function getAssignedLoadForUser() {
    return authWrapper(get, '/loads/active');
}

export function goToNextLoadState() {
    return authWrapper(patch, '/loads/active/state');
}
