import authWrapper from './utils/auth_wrapper';
import { delete_, patch } from './fetch';
import { getAllLoadsForUser, getAssignedLoadForUser } from './loads_service';

export function changePassword({ oldPassword, newPassword }) {
    return authWrapper(patch, '/users/me', {
        body: {
            newPassword, oldPassword,
        },
    });
}

export function deleteUserProfile() {
    return authWrapper(delete_, '/users/me');
}

export function getCurrentDriversForShipper() {
    return getAllLoadsForUser({ status: 'ASSIGNED' })
        // eslint-disable-next-line camelcase
        .then((loads) => (loads.length ? loads.map(({ name, assigned_to }) => ({
            id: assigned_to,
            loadName: name,
        })) : null));
}

export function getCurrentShippersForDriver() {
    return getAssignedLoadForUser()
        .then(({ load }) => (load ? [{
            id: load.created_by,
            loadName: load.name,
        }] : null));
}
