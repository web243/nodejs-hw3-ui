import { getUserFromSessionStorage } from '../../store/user_session_storage';

export default function authWrapper(callback, uri, params,
    token = getUserFromSessionStorage().token) {
    const headers = {
        Authorization: `Bearer ${token}`,
    };
    return callback(uri, { headers, ...params })
        .then(async (result) => ({
            status: result.status,
            body: await result.json(),
        }))
        .then(({ body }) => {
            const { message } = body;
            if (message) {
                alert(message);
            }
            return body;
        })
        .catch((error) => alert(error));
}
