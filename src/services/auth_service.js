import { getJson, post } from './fetch';

export function logInUser({ email, password }) {
    return new Promise((resolve, reject) => post('/auth/login', { body: { email, password } })
        .then((result) => result.json())
        .then((body) => {
            const token = body.jwt_token;
            if (!token) {
                reject(new Error(body.message));
            }
            return token;
        })
        .then((token) => {
            const headers = {
                Authorization: `Bearer ${token}`,
            };
            return getJson('/users/me', { headers })
                .then(({ user }) => Object.assign(user, { token }));
        })
        .then((user) => {
            resolve(user);
        })
        .catch((e) => {
            reject(e);
        }));
}

export function signUpUser({ email, password, role }) {
    return new Promise((resolve, reject) => post('/auth/register',
        { body: { email, password, role } })
        .then(async (result) => ({
            status: result.status,
            body: await result.json(),
        }))
        .then(({ status, body }) => {
            if (status === 200) {
                resolve();
            }
            reject(new Error(body.message));
        })
        .catch((e) => {
            reject(e);
        }));
}
