import authWrapper from './utils/auth_wrapper';
import { post } from './fetch';

export function getChat(participants) {
    return authWrapper(post, '/chats', { body: { participants } });
}

export function addChatMessages(participants, messages) {
    return authWrapper(post, '/chats', { body: { participants } });
}
