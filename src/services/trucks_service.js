import authWrapper from './utils/auth_wrapper';
import {
    delete_,
    get, post, put,
} from './fetch';

const trucksInfo = {
    SPRINTER: {
        dimensions: {
            width: 300,
            length: 250,
            height: 170,
        },
        payload: 1700,
    },
    'SMALL STRAIGHT': {
        dimensions: {
            width: 500,
            length: 250,
            height: 170,
        },
        payload: 2500,
    },
    'LARGE STRAIGHT': {
        dimensions: {
            width: 700,
            length: 350,
            height: 200,
        },
        payload: 4000,
    },
};

export function getAssignedTruckForUser() {
    return authWrapper(get, '/trucks/active');
}

export function getAllTrucksForUser() {
    return authWrapper(get, '/trucks');
}

export function getTruckByIdForUser(truckId) {
    return authWrapper(get, `/trucks/${truckId}`)
        .then(({ truck }) => {
            truck.dimensions = trucksInfo[truck.type].dimensions;
            truck.payload = trucksInfo[truck.type].payload;
            return truck;
        })
        .catch((error) => alert(error));
}

export function addTruckToUser(data) {
    return authWrapper(post, '/trucks', { body: data });
}

export function updateTruckForUser(truck) {
    return authWrapper(put, `/trucks/${truck._id}`, { body: truck });
}

export function deleteTruckByIdForUser(truckId) {
    return authWrapper(delete_, `/trucks/${truckId}`);
}

export function assignTruckForUser(truckId) {
    return authWrapper(post, `/trucks/${truckId}/assign`);
}
