import SERVER_URL from './url';

function getUrl(uri) {
    return `${SERVER_URL}${uri}`;
}

function fetchByMethod({ uri, params, method }) {
    const { headers, body } = params;
    const url = getUrl(uri);
    return fetch(url, {
        method,
        body: JSON.stringify(body),
        headers: { 'Content-type': 'application/json; charset=UTF-8', ...headers },
    });
}

export function get(uri, params) {
    const { headers, encode } = params;
    let url = getUrl(uri);
    if (encode) {
        url = encodeURI(url);
    }
    return fetch(url, {
        method: 'GET',
        headers,
    });
}

export function getJson(uri, params) {
    return get(uri, params)
        .then((response) => response.json());
}

export function patch(uri, params) {
    return fetchByMethod({ uri, params, method: 'PATCH' });
}

export function put(uri, params) {
    return fetchByMethod({ uri, params, method: 'PUT' });
}

export function post(uri, params) {
    return fetchByMethod({ uri, params, method: 'POST' });
}

// eslint-disable-next-line no-underscore-dangle
export function delete_(uri, params) {
    const { headers } = params;
    const url = getUrl(uri);
    return fetch(url, {
        method: 'DELETE',
        headers: { 'Content-type': 'application/json', ...headers },
    });
}
