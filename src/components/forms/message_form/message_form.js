import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../buttons/button';
import './message_form.css';

function MessageForm(props) {
    const { onSubmit } = props;
    const { t } = useTranslation();

    const form = (
        <Form className="chat-message-form">
            <Field type="text" name="message" placeholder={t('actions.typeMessage')} />
            <Button type="submit" text="actions.send" />
        </Form>
    );
    return (
        <Formik
            initialValues={{
                message: '',
            }}
            onSubmit={(values, { resetForm }) => {
                const { message } = values;
                if (message) {
                    onSubmit(message);
                }
                resetForm();
            }}
        >
            {form}
        </Formik>
    );
}

export default MessageForm;
