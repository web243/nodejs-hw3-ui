import { Formik } from 'formik';
import React from 'react';
import { addTruckToUser } from '../../../services/trucks_service';
import TruckForm from './truck_form';

function TruckCreatorFormWithFormik() {
    return (
        <Formik
            initialValues={{
                type: 'SPRINTER',
            }}
            onSubmit={async (values) => {
                await addTruckToUser(values);
            }}
        >
            <TruckForm />
        </Formik>
    );
}

export default TruckCreatorFormWithFormik;
