import React from 'react';
import {
    Field, Form,
} from 'formik';
import { useTranslation } from 'react-i18next';
import Button from '../../buttons/button';

function TruckForm() {
    const { t } = useTranslation();
    return (
        <Form className="editor-form">
            <div className="text-input-container">
                <label htmlFor="type">{t('general.type')}</label>
                <Field as="select" id="type" name="type" required>
                    <option value="SPRINTER">
                        {t('info.truck.types.sprinter')}
                    </option>
                    <option value="SMALL STRAIGHT">
                        {t('info.truck.types.smallStraight')}
                    </option>
                    <option value="LARGE STRAIGHT">
                        {t('info.truck.types.largeStraight')}
                    </option>
                </Field>
            </div>
            <Button
                type="submit"
                text="actions.save"
            />
        </Form>
    );
}

export default TruckForm;
