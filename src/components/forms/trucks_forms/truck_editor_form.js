import { Formik } from 'formik';
import React from 'react';
import { updateTruckForUser } from '../../../services/trucks_service';
import TruckForm from './truck_form';

function TruckEditorFormWithFormik(props) {
    const { truck } = props;
    return (
        <Formik
            initialValues={{
                // eslint-disable-next-line no-underscore-dangle
                _id: truck._id,
                type: truck.type,
            }}
            onSubmit={async (values) => {
                await updateTruckForUser(values);
            }}
        >
            <TruckForm />
        </Formik>
    );
}

export default TruckEditorFormWithFormik;
