import React from 'react';
import './text_input.css';
import { useTranslation } from 'react-i18next';

function Input(props) {
    const { t } = useTranslation();
    const {
        onChange, name, label, placeholder, required, type, step, min, className,
    } = props;
    const id = name || label;
    return (
        <div className="text-input-container">
            <label htmlFor={id}>
                {t(label)}
            </label>
            {className === 'textarea'
                ? (
                    <textarea
                        id={id}
                        name={id}
                        onChange={onChange || null}
                        required={required}
                    />
                )
                : (
                    <input
                        placeholder={placeholder || ''}
                        id={id}
                        name={id}
                        type={type || 'text'}
                        onChange={onChange || null}
                        step={step || null}
                        min={min || null}
                        required={required}
                    />
                )}
        </div>
    );
}

export default Input;
