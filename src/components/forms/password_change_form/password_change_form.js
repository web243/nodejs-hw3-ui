import React from 'react';
import {
    Field, Form, Formik,
} from 'formik';
import { useTranslation } from 'react-i18next';
import Button from '../../buttons/button';
import { changePassword } from '../../../services/users_service';

function PasswordChangeForm() {
    const { t } = useTranslation();
    return (
        <Form className="editor-form">
            <div className="text-input-container">
                <label htmlFor="oldPassword">{t('profile.passwords.old')}</label>
                <Field id="oldPassword" name="oldPassword" type="password" required />
            </div>
            <div className="text-input-container">
                <label htmlFor="newPassword">{t('profile.passwords.new')}</label>
                <Field id="newPassword" name="newPassword" type="password" required />
            </div>
            <Button type="submit" text="actions.save" />
        </Form>
    );
}

function PasswordChangeFormWithFormik() {
    return (
        <Formik
            initialValues={{
                // eslint-disable-next-line no-underscore-dangle
                oldPassword: '',
                newPassword: '',
            }}
            onSubmit={async (values) => {
                await changePassword(values);
            }}
        >
            <PasswordChangeForm />
        </Formik>
    );
}

export default PasswordChangeFormWithFormik;
