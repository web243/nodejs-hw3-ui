import {
    Formik,
} from 'formik';
import React from 'react';
import { updateLoadForUser } from '../../../services/loads_service';
import LoadForm from './load_form';

import './load_forms.css';

function LoadEditorFormWithFormik(props) {
    const { load } = props;
    const { dimensions } = load;
    return (
        <Formik
            initialValues={{
                // eslint-disable-next-line no-underscore-dangle
                id: load._id,
                name: load.name,
                pickupAddress: load.pickup_address,
                deliveryAddress: load.delivery_address,
                payload: load.payload,
                width: dimensions.width,
                height: dimensions.height,
                length: dimensions.length,
            }}
            onSubmit={async (values) => {
                await updateLoadForUser(values);
            }}
        >
            <LoadForm />
        </Formik>
    );
}

export default LoadEditorFormWithFormik;
