import { Formik } from 'formik';
import React from 'react';
import { addLoadToUser } from '../../../services/loads_service';
import LoadForm from './load_form';

function LoadCreatorFormWithFormik() {
    return (
        <Formik
            initialValues={{
                // eslint-disable-next-line no-underscore-dangle
                name: '',
                pickupAddress: '',
                deliveryAddress: '',
                payload: null,
                width: null,
                height: null,
                length: null,
            }}
            onSubmit={async (values) => {
                await addLoadToUser(values);
            }}
        >
            <LoadForm />
        </Formik>
    );
}

export default LoadCreatorFormWithFormik;
