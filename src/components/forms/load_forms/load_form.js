import React from 'react';
import './load_forms.css';
import {
    Field, Form,
} from 'formik';
import { useTranslation } from 'react-i18next';
import Button from '../../buttons/button';

function LoadForm() {
    const { t } = useTranslation();
    const dimensionsInputs = (
        <div className="dimensions-inputs">
            <div className="text-input-container">
                <label htmlFor="loadWidth">
                    {t('info.general.size.width')}
                </label>
                <Field
                    id="loadWidth"
                    type="number"
                    name="width"
                    min={1}
                    required
                />
            </div>
            <div className="text-input-container">
                <label htmlFor="loadLength">
                    {t('info.general.size.length')}
                </label>
                <Field
                    id="loadLength"
                    type="number"
                    name="length"
                    min={1}
                    required
                />
            </div>
            <div className="text-input-container">
                <label htmlFor="loadHeight">
                    {t('info.general.size.height')}
                </label>
                <Field
                    id="loadHeight"
                    type="number"
                    name="height"
                    min={1}
                    required
                />
            </div>
        </div>
    );
    return (
        <Form className="editor-form load-form">
            <div className="load-form-inputs-container">
                <fieldset>
                    <legend>{t('info.generalInfo')}</legend>
                    <div className="text-input-container">
                        <label htmlFor="loadName">
                            {t('info.load.name')}
                        </label>
                        <Field
                            id="loadName"
                            type="text"
                            name="name"
                            required
                        />
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{t('info.load.addresses')}</legend>
                    <div className="text-input-container">
                        <label htmlFor="pu_address">
                            {t('info.load.pickupAddress')}
                        </label>
                        <Field
                            id="pu_address"
                            type="text"
                            name="pickupAddress"
                            required
                        />
                    </div>
                    <div className="text-input-container">
                        <label htmlFor="d_address">
                            {t('info.load.deliveryAddress')}
                        </label>
                        <Field
                            id="d_address"
                            type="text"
                            name="deliveryAddress"
                            required
                        />
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{t('info.general.size.size')}</legend>
                    <div className="text-input-container">
                        <label htmlFor="loadPayload">
                            {t('info.general.size.payload')}
                        </label>
                        <Field
                            id="loadPayload"
                            type="number"
                            name="payload"
                            min={1}
                            step={0.001}
                            required
                        />
                    </div>
                    {dimensionsInputs}
                </fieldset>
            </div>
            <div className="buttons-container">
                <Button
                    type="reset"
                    text="actions.reset"
                    className="button-light"
                />
                <Button
                    type="submit"
                    text="actions.save"
                />
            </div>
        </Form>
    );
}

export default LoadForm;
