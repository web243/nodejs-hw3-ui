import React from 'react';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import Input from '../text_input/text_input';
import { logIn } from '../../../store/reducers/user_reducer';
import Schema from './validation_schema';
import Button from '../../buttons/button';
import './auth_form.css';

function LoginForm(props) {
    const {
        values, handleChange, handleSubmit,
    } = props.formik;
    return (
        <form className="editor-form auth-form" onSubmit={handleSubmit}>
            <Input
                label="auth.email"
                type="text"
                name="email"
                onChange={handleChange}
                value={values.email}
                required
            />
            <Input
                label="auth.password"
                type="password"
                name="password"
                onChange={handleChange}
                value={values.password}
                required
            />
            <Button
                type="submit"
                text="auth.logIn"
            />
        </form>
    );
}

function LoginFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(logIn(values));
        },
    });
    return <LoginForm formik={formik} />;
}

export default LoginFormWithFormik;
