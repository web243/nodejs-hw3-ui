import React from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import Button from '../../buttons/button';
import Input from '../text_input/text_input';
import { registerUser } from '../../../store/reducers/user_reducer';
import Schema from './validation_schema';
import './auth_form.css';
import ValidationErrorBlock from '../../blocks/validation_error_block';

function RegisterForm(props) {
    const { formik } = props;
    const {
        values, errors, handleChange, handleSubmit,
    } = formik;
    const { t } = useTranslation();
    return (
        <form className="editor-form auth-form" onSubmit={handleSubmit}>
            <Input
                label="auth.email"
                name="email"
                type="text"
                onChange={handleChange}
                value={values.email}
                required
            />
            <div className="text-input-container">
                <label htmlFor="role-select">
                    {t('general.role')}
                </label>
                <select name="role" id="role-select" value={values.role} onChange={handleChange} required>
                    <option value="" selected>{t('actions.selectRole')}</option>
                    <option value="DRIVER">{t('general.driver')}</option>
                    <option value="SHIPPER">{t('general.shipper')}</option>
                </select>
            </div>
            <Input
                label="auth.password"
                name="password"
                type="password"
                onChange={handleChange}
                value={values.password}
                required
            />
            <Input
                label="auth.confirmPassword"
                name="confirmPassword"
                type="password"
                onChange={handleChange}
                value={values.confirmPassword}
                required
            />
            {errors.email && <ValidationErrorBlock message={errors.email} />}
            {errors.password && <ValidationErrorBlock message={errors.password} />}
            {errors.confirmPassword && <ValidationErrorBlock message={errors.confirmPassword} />}
            <Button type="submit" text="auth.signUp" />
        </form>
    );
}

function RegisterFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            confirmPassword: '',
            role: '',
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(registerUser(values));
        },
    });
    return <RegisterForm formik={formik} />;
}

export default RegisterFormWithFormik;
