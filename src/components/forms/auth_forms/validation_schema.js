import * as Yup from 'yup';

const Schema = Yup.object().shape({
    email: Yup
        .string()
        .email()
        .required(),
    password: Yup
        .string(),
    confirmPassword: Yup.string().when('password', {
        is: (val) => (!!(val && val.length > 0)),
        then: Yup.string().oneOf(
            [Yup.ref('password')],
            'auth.messages.passwordsNotMatch',
        ),
    }),
});

export default Schema;
