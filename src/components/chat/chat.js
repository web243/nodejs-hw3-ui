import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import MessageForm from '../forms/message_form/message_form';
import socket from '../../socket';
import mapStateToProps from '../../store/map_state_to_props';
import { getChat } from '../../services/chats_service';
import GlobalErrorBlock from '../blocks/global_error_block';
import TextBlock from '../blocks/message_block';
import Message from './message';

function Chat(props) {
    const { messages, onSubmit } = props;

    return (
        <div className="chat-container">
            <div className="chat">
                <ul className="chat-messages">
                    {messages}
                </ul>
                <MessageForm onSubmit={onSubmit} />
            </div>
        </div>
    );
}

function ChatWithSockets(props) {
    const { self, otherUser, chatHistory } = props;
    const [messages, setMessages] = useState([]);

    const onSubmit = (content) => {
        socket.emit('private message', {
            content,
            to: otherUser.id,
        });
        const message = <Message content={content} user={self} fromSelf />;
        setMessages((oldMessages) => oldMessages.concat([message]));
    };

    useEffect(() => {
        chatHistory.forEach((message) => {
            const { content, from } = message;
            const messageElement = (
                <Message
                    content={content}
                    user={otherUser}
                    fromSelf={from === self.id}
                />
            );
            setMessages((oldMessages) => oldMessages.concat([messageElement]));
        });
    }, [chatHistory]);

    useEffect(() => {
        socket.auth = { userId: self.id };
        socket.connect();

        socket.on('private message', ({ content, from }) => {
            const message = (
                <Message
                    content={content}
                    user={otherUser}
                    fromSelf={from === self.id}
                />
            );
            setMessages((oldMessages) => oldMessages.concat([message]));
        });

        return () => {
            socket.off('private message');
            socket.off('connect_error');
            socket.disconnect();
        };
    });

    return <Chat messages={messages} onSubmit={onSubmit} />;
}

function ChatWithFetch(props) {
    const { self, otherUser } = props;
    const [chatHistory, setChatHistory] = useState([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        getChat([self.id, otherUser.id])
            .then(
                (result) => {
                    setIsLoaded(true);
                    setChatHistory(result.chat);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="state.loading" />;
    }
    return <ChatWithSockets self={self} otherUser={otherUser} chatHistory={chatHistory} />;
}
export default connect(mapStateToProps)(ChatWithFetch);
