import React from 'react';
import { useTranslation } from 'react-i18next';

function Message(props) {
    const { content, fromSelf, user } = props;
    const { t } = useTranslation();
    return (
        <li className={fromSelf ? 'your-message' : 'other-user-message'}>
            <p className="chat-name">
                {fromSelf ? t('general.yourself')
                    : t(`general.${user.role.toLowerCase()}`)}
            </p>
            <p>{content}</p>
        </li>
    );
}

export default Message;
