import React from 'react';
import './chat.css';
import { connect } from 'react-redux';
import Chat
    from './chat';
import mapStateToProps from '../../store/map_state_to_props';

function ChatWrapper(props) {
    const { user, id, role } = props;
    const self = {
        // eslint-disable-next-line no-underscore-dangle
        id: user.id,
        role: user.role,
    };
    const otherUser = {
        id,
        role,
    };
    return <Chat self={self} otherUser={otherUser} />;
}

export default connect(mapStateToProps)(ChatWrapper);
