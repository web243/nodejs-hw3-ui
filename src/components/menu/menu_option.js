import { NavLink, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import React from 'react';

export default function MenuOption(props) {
    const { to, label } = props;
    const { t } = useTranslation();
    const location = useLocation();
    return (
        <NavLink
            key={to}
            to={to}
            exact
            isActive={() => `${location.pathname}${location.search}` === to}
            activeClassName="menu-option-selected"
        >
            {t(`menu.${label}`)}
        </NavLink>
    );
}
