import React from 'react';
import MenuOption from './menu_option';

const driverLinksData = [
    {
        to: '/trucks',
        label: 'trucks',
    },
    {
        to: '/loads/assigned',
        label: 'assignedLoad',
    },
    {
        to: '/trucks/assigned',
        label: 'assignedTruck',
    },
];

const driverLinks = driverLinksData.map((data) => (
    <MenuOption
        to={data.to}
        label={data.label}
    />
));

export default driverLinks;
