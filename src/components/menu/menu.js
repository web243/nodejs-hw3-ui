import React from 'react';
import { connect } from 'react-redux';
import mapStateToProps from '../../store/map_state_to_props';
import shipperLinks from './shipper_links';
import './menu.css';
import MenuOption from './menu_option';
import ButtonWithLink from '../buttons/button_with_link';
import driverLinks from './driver_links';

function getLinks(userRole) {
    return userRole === 'SHIPPER' ? shipperLinks : driverLinks;
}

function getButton(userRole) {
    return userRole === 'SHIPPER' ? (
        <ButtonWithLink
            text="CRUD.load.add"
            path="/loads/create"
        />
    ) : (
        <ButtonWithLink
            text="CRUD.truck.add"
            path="/trucks/create"
        />
    );
}

function Menu(props) {
    const { user } = props;
    const links = getLinks(user.role);
    const button = getButton(user.role);
    return (
        <nav className="menu">
            <div className="menu-options-container">
                {links}
                <MenuOption key="chat" to="/choose_chat" label="chat" />
                <MenuOption key="profile" to="/profile" label="profile" />
            </div>
            {button}
        </nav>
    );
}

export default connect(mapStateToProps)(Menu);
