import React from 'react';
import MenuOption from './menu_option';

const shipperLinksData = [
    {
        to: '/loads?status=NEW',
        label: 'newLoads',
    },
    {
        to: '/loads?status=POSTED',
        label: 'postedLoads',
    },
    {
        to: '/loads?status=ASSIGNED',
        label: 'assignedLoads',
    },
    {
        to: '/loads?status=SHIPPED',
        label: 'history',
    },
];

const shipperLinks = shipperLinksData.map((data) => (
    <MenuOption
        to={data.to}
        label={data.label}
    />
));

export default shipperLinks;
