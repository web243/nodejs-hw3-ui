import React from 'react';
import { useTranslation } from 'react-i18next';
import { isEditable, getButtons } from './utils';

function LoadVerticalTable(props) {
    const { load } = props;
    const { t } = useTranslation();
    const ableToEdit = isEditable(load.status);
    const buttons = getButtons(load._id, ableToEdit);
    return (
        <div className="load-vertical-table-container">
            <table className="load-vertical-table">
                <tr key={load.name}>
                    <th>{t('info.load.name')}</th>
                    <td>{load.name}</td>
                </tr>
                <tr key={load.created_date}>
                    <th>{t('info.general.createdDate')}</th>
                    <td>{load.created_date}</td>
                </tr>
                <tr key={load.pickup_address}>
                    <th>{t('info.load.pickupAddress')}</th>
                    <td>{load.pickup_address}</td>
                </tr>
                <tr key={load.delivery_address}>
                    <th>{t('info.load.deliveryAddress')}</th>
                    <td>{load.delivery_address}</td>
                </tr>
            </table>
            {buttons}
        </div>
    );
}

function LoadsVerticalTables(props) {
    const { loads } = props;
    return (
        <React.Fragment>
            {loads.map((load) => <LoadVerticalTable load={load} />)}
        </React.Fragment>
    );
}

export default LoadsVerticalTables;
