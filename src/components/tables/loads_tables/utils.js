import React from 'react';
import ShowLoadButton
    from '../../buttons/types/icons_buttons/tool_buttons/loads_buttons/show_load_button';
import EditLoadButton
    from '../../buttons/types/icons_buttons/tool_buttons/loads_buttons/edit_load_button';
import PostLoadButton
    from '../../buttons/types/icons_buttons/tool_buttons/loads_buttons/post_load_button';
import DeleteLoadButton
    from '../../buttons/types/icons_buttons/tool_buttons/loads_buttons/delete_load_button';

export function isEditable(status) {
    return status.toUpperCase() === 'NEW';
}

export function getButtons(loadId, ableToEdit) {
    return (
        <div className="tools-buttons-container">
            <ShowLoadButton id={loadId} />
            {ableToEdit && <EditLoadButton id={loadId} />}
            {ableToEdit && <PostLoadButton id={loadId} />}
            {ableToEdit && <DeleteLoadButton id={loadId} />}
        </div>
    );
}
