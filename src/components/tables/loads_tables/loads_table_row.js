import React from 'react';
import { isEditable, getButtons } from './utils';

function LoadTableRow(props) {
    const { load } = props;
    const ableToEdit = isEditable(load.status);
    const buttons = getButtons(load._id, ableToEdit);
    return (
        // eslint-disable-next-line no-underscore-dangle
        <tr id={load._id} key={load._id}>
            <td key={load.name}>{load.name}</td>
            <td key={load.created_date}>{load.created_date}</td>
            <td key={load.pickup_address}>{load.pickup_address}</td>
            <td key={load.delivery_address}>{load.delivery_address}</td>
            <td key={load._id}>{buttons}</td>
        </tr>
    );
}

export default LoadTableRow;
