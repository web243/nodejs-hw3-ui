import React, { useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';
import { getAllLoadsForUser } from '../../../services/loads_service';
import '../tables.css';
import './loads_table.css';
import LoadsVerticalTables from './loads_vertical_tables';
import LoadsHorizontalTable from './loads_horizontal_table';
import NoLoadsFoundMessage from '../../blocks/messages/no_loads_found_message';

function LoadsTable(props) {
    const { loads } = props;
    const isMobile = useMediaQuery({ query: '(max-width: 900px)' });
    if (loads && loads.length > 0) {
        return isMobile ? <LoadsVerticalTables loads={loads} />
            : <LoadsHorizontalTable loads={loads} />;
    }
    return <NoLoadsFoundMessage />;
}

function LoadsTableWithFetch(props) {
    const { status } = props;
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [loads, setLoads] = useState([]);

    useEffect(() => {
        getAllLoadsForUser({ status })
            .then(
                (result) => {
                    setIsLoaded(true);
                    setLoads(result);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, [status]);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return <LoadsTable loads={loads} />;
}

export default LoadsTableWithFetch;
