import React from 'react';
import { useTranslation } from 'react-i18next';
import LoadTableRow from './loads_table_row';

function LoadsHorizontalTable(props) {
    const { loads } = props;
    const { t } = useTranslation();
    return (
        <table>
            <thead>
                <tr>
                    <th key="name">{t('info.load.name')}</th>
                    <th key="created_date">{t('info.general.createdDate')}</th>
                    <th key="pu_address">{t('info.load.pickupAddress')}</th>
                    <th key="d_address">{t('info.load.deliveryAddress')}</th>
                </tr>
            </thead>
            <tbody>
                {loads.map((load) => <LoadTableRow load={load} />)}
            </tbody>
        </table>
    );
}

export default LoadsHorizontalTable;
