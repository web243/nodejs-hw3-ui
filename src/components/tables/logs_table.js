import React from 'react';
import { useTranslation } from 'react-i18next';
import NoDataYetMessage from '../blocks/messages/no_data_yet_message';

function LogsTable(props) {
    const { logs } = props;
    const { t } = useTranslation();
    return logs && logs.length > 0 ? (
        <table>
            <thead>
                <tr>
                    <th>{t('general.date')}</th>
                    <th>{t('general.message')}</th>
                </tr>
            </thead>
            <tbody>
                {logs.map(({ message, time }) => (
                    <tr>
                        <td>{time}</td>
                        <td>{message}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    ) : <NoDataYetMessage data="message" />;
}

export default LogsTable;
