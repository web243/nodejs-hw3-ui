import React, { useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';
import { getAllTrucksForUser } from '../../../services/trucks_service';
import '../tables.css';
import TrucksVerticalTables from './trucks_vertical_tables';
import TrucksHorizontalTable from './trucks_horizontal_table';
import { canUserAssign } from './utils';
import NoTrucksFoundMessage from '../../blocks/messages/no_trucks_found_message';

function TrucksTable(props) {
    const { trucks, ableToAssign } = props;
    const isMobile = useMediaQuery({ query: '(max-width: 900px)' });
    if (trucks && trucks.length > 0) {
        return isMobile ? <TrucksVerticalTables trucks={trucks} ableToAssign={ableToAssign} />
            : <TrucksHorizontalTable trucks={trucks} ableToAssign={ableToAssign} />;
    }
    return <NoTrucksFoundMessage />;
}

function TrucksTableWithFetch() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [trucks, setTrucks] = useState([]);
    const [ableToAssign, setAbleToAssign] = useState(false);

    useEffect(() => {
        getAllTrucksForUser()
            .then(
                (result) => {
                    setTrucks(result.trucks);
                    return canUserAssign();
                },
            )
            .then(
                (canAssign) => {
                    setAbleToAssign(canAssign);
                    setIsLoaded(true);
                },
                (e) => {
                    setError(e);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return <TrucksTable trucks={trucks} ableToAssign={ableToAssign} />;
}

export default TrucksTableWithFetch;
