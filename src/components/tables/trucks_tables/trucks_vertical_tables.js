import React from 'react';
import { useTranslation } from 'react-i18next';
import { getButtons } from './utils';
import localizationMap from '../../../localization/utils';

function TruckVerticalTable(props) {
    const { truck } = props;
    const buttons = getButtons(props);
    const { t } = useTranslation();
    return (
        <div className="load-vertical-table-container">
            <table className="load-vertical-table">
                <tr key={truck.type}>
                    <th>{t('general.type')}</th>
                    <td>{t(localizationMap[truck.type])}</td>
                </tr>
                <tr key={truck.status}>
                    <th>{t('general.status')}</th>
                    <td>{t(localizationMap[truck.status]) || '-'}</td>
                </tr>
                <tr key={truck.created_date}>
                    <th>{t('info.general.createdDate')}</th>
                    <td>{truck.created_date}</td>
                </tr>
            </table>
            {buttons}
        </div>
    );
}

function TrucksVerticalTables(props) {
    const { trucks, ableToAssign } = props;
    return (
        <React.Fragment>
            {trucks.map((truck) => (
                <TruckVerticalTable
                    truck={truck}
                    ableToAssign={ableToAssign}
                />
            ))}
        </React.Fragment>
    );
}

export default TrucksVerticalTables;
