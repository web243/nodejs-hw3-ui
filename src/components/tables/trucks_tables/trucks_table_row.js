import React from 'react';
import { useTranslation } from 'react-i18next';
import { getButtons } from './utils';
import localizationMap from '../../../localization/utils';

function TruckTableRow(props) {
    const { truck } = props;
    const { t } = useTranslation();
    const buttons = getButtons(props);
    return (
        // eslint-disable-next-line no-underscore-dangle
        <tr id={truck._id} key={truck._id}>
            <td key={truck.type}>{t(localizationMap[truck.type])}</td>
            <td key={truck.status}>{t(localizationMap[truck.status]) || '-'}</td>
            <td key={truck.created_date}>{truck.created_date}</td>
            <td key={truck._id}>{buttons}</td>
        </tr>
    );
}

export default TruckTableRow;
