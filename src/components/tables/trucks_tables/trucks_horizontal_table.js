import React from 'react';
import { useTranslation } from 'react-i18next';
import TruckTableRow from './trucks_table_row';

function TrucksHorizontalTable(props) {
    const { trucks, ableToAssign } = props;
    const { t } = useTranslation();
    return (
        <table>
            <thead>
                <tr>
                    <th key="type">{t('general.type')}</th>
                    <th key="status">{t('general.status')}</th>
                    <th key="created_date">{t('info.general.createdDate')}</th>
                </tr>
            </thead>
            <tbody>
                {trucks.map((truck) => <TruckTableRow truck={truck} ableToAssign={ableToAssign} />)}
            </tbody>
        </table>
    );
}

export default TrucksHorizontalTable;
