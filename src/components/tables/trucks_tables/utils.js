import React from 'react';
import { getAssignedTruckForUser } from '../../../services/trucks_service';
import ShowTruckButton
    from '../../buttons/types/icons_buttons/tool_buttons/trucks_buttons/show_truck_button';
import EditTruckButton
    from '../../buttons/types/icons_buttons/tool_buttons/trucks_buttons/edit_truck_button';
import AssignTruckButton
    from '../../buttons/types/icons_buttons/tool_buttons/trucks_buttons/assign_truck_button';
import DeleteTruckButton
    from '../../buttons/types/icons_buttons/tool_buttons/trucks_buttons/delete_truck_button';

function isEditable(truck) {
    return !truck.assigned_to;
}

export async function canUserAssign() {
    const data = await getAssignedTruckForUser();
    return data ? !data.truck : true;
}

export function getButtons({ truck, ableToAssign }) {
    const ableToEdit = isEditable(truck);
    console.log(truck._id);
    return (
        <div className="tools-buttons-container">
            <ShowTruckButton id={truck._id} />
            {ableToEdit && <EditTruckButton id={truck._id} />}
            {ableToAssign && <AssignTruckButton id={truck._id} />}
            {ableToEdit && <DeleteTruckButton id={truck._id} />}
        </div>
    );
}
