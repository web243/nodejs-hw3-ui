import React from 'react';
import { useTranslation } from 'react-i18next';
import DossierTable from './dossier_table';
import localizationMap from '../../../localization/utils';

function LoadDossier(props) {
    const { load } = props;
    if (!load) {
        return <p className="message">No load found</p>;
    }
    const { t } = useTranslation();
    const { dimensions } = load;
    const data = {
        'info.general.createdDate': load.created_date,
        'general.status': t(localizationMap[load.status]),
        'general.state': t(localizationMap[load.state]),
        'info.load.pickupAddress': load.pickup_address,
        'info.load.deliveryAddress': load.delivery_address,
        'info.general.size.dimensions': `${dimensions.width} x ${dimensions.length} x ${dimensions.height}`,
        'info.general.size.payload': load.payload,
    };
    return <DossierTable data={data} />;
}

export default LoadDossier;
