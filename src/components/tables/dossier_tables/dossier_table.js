import React from 'react';
import '../tables.css';
import { useTranslation } from 'react-i18next';
import NoDataFoundMessage from '../../blocks/messages/no_data_found_message';

function DossierTable(props) {
    const { data } = props;
    const { t } = useTranslation();
    const rows = Object.entries(data).map(([key, value]) => key && value && (
        <tr>
            <th>{t(key)}</th>
            <td>{value}</td>
        </tr>
    ));
    return data ? (
        <table className="vertical-headers-table dossier-table">
            <tbody>
                {rows}
            </tbody>
        </table>
    ) : <NoDataFoundMessage />;
}

export default DossierTable;
