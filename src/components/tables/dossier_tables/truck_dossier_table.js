import React from 'react';
import { useTranslation } from 'react-i18next';
import DossierTable from './dossier_table';
import localizationMap from '../../../localization/utils';
import NoDataFoundMessage from '../../blocks/messages/no_data_found_message';

function TruckDossier(props) {
    const { truck } = props;
    if (!truck) {
        return <NoDataFoundMessage />;
    }
    const { t } = useTranslation();
    const { dimensions } = truck;
    const data = {
        'general.type': t(localizationMap[truck.type]),
        'info.general.createdDate': truck.created_date,
        'general.status': t(localizationMap[truck.status]),
        'info.general.size.payload': truck.payload,
        'info.general.size.dimensions': dimensions
            && `${dimensions.width} x ${dimensions.length} x ${dimensions.height}`,
    };
    return <DossierTable data={data} />;
}

export default TruckDossier;
