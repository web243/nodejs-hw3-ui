import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import LoginFormWithFormik from '../../forms/auth_forms/login_form';
import RegisterButton from '../../buttons/types/auth_buttons/register_button';
import './auth_page.css';
import Logo from '../../logo/logo';
import mapStateToProps from '../../../store/map_state_to_props';
import LanguagesButtons from '../../buttons/types/languages_buttons/languages_buttons';
import { useTranslation } from 'react-i18next';

function LoginPage(props) {
    const { user } = props;
    const { t } = useTranslation();
    return user ? <Redirect to="/" /> : (
        <main className="auth-page login-page">
            <div className="auth-header">
                <Logo />
                <LanguagesButtons />
            </div>
            <LoginFormWithFormik />
            <p>{t('auth.messages.noAccountYet')}</p>
            <RegisterButton />
        </main>
    );
}

export default connect(mapStateToProps)(LoginPage);
