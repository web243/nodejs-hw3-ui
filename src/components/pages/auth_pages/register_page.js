import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import RegisterForm from '../../forms/auth_forms/register_form';
import Logo from '../../logo/logo';
import mapStateToProps from '../../../store/map_state_to_props';
import LoginButton from '../../buttons/types/auth_buttons/login_button';
import LanguagesButtons from '../../buttons/types/languages_buttons/languages_buttons';

function RegisterPage(props) {
    const { user } = props;
    const { t } = useTranslation();
    return user ? <Redirect to="/" /> : (
        <main className="auth-page">
            <div className="auth-header">
                <Logo />
                <LanguagesButtons />
            </div>
            <RegisterForm />
            <p>{t('auth.messages.haveAnAccountAlready')}</p>
            <LoginButton />
        </main>
    );
}

export default connect(mapStateToProps)(RegisterPage);
