import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import GlobalErrorBlock from '../../../blocks/global_error_block';
import TextBlock from '../../../blocks/message_block';
import mapStateToProps from '../../../../store/map_state_to_props';
import {
    getCurrentDriversForShipper,
    getCurrentShippersForDriver,
} from '../../../../services/users_service';
import ButtonWithLink from '../../../buttons/button_with_link';
import NoDataFoundMessage from '../../../blocks/messages/no_data_found_message';
import './choose_chat_page.css';

async function getUsersForChat(self) {
    const { role } = self;
    if (role === 'SHIPPER') {
        const drivers = await getCurrentDriversForShipper();
        return drivers ? drivers.map(({ id, loadName }) => ({
            id,
            loadName,
            role: 'DRIVER',
        })) : [];
    }
    const shippers = await getCurrentShippersForDriver();
    return shippers ? shippers.map(({ id, loadName }) => ({
        id,
        loadName,
        role: 'SHIPPER',
    })) : [];
}

function ChooseChatPage(props) {
    const { user } = props;
    const self = {
        id: user.id,
        role: user.role,
    };
    const [otherUsers, setOtherUsers] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
    useEffect(() => {
        getUsersForChat(self)
            .then(
                (result) => {
                    setOtherUsers(result);
                    console.log(result);
                    setIsLoaded(true);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="state.loading" />;
    }
    return (
        <main className="choose-chat-page">
            {otherUsers && otherUsers.length ? (
                <ul>
                    {otherUsers.map((otherUser) => (
                        <li>
                            {otherUser.loadName}
                            <ButtonWithLink
                                path={`/chat?id=${otherUser.id}&role=${otherUser.role}`}
                                text="actions.join"
                            />
                        </li>
                    ))}
                </ul>
            ) : <NoDataFoundMessage />}
        </main>
    );
}

export default connect(mapStateToProps)(ChooseChatPage);
