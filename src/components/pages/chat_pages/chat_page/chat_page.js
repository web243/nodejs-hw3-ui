import React, { useEffect } from 'react';
import Scroll from 'react-scroll';
import { StringParam, useQueryParam } from 'use-query-params';
import ChatWrapper from '../../../chat/chat_wrapper';
import Button from '../../../buttons/button';
import './chat_page.css';

const scroll = Scroll.animateScroll;

function ChatPage() {
    const [id] = useQueryParam('id', StringParam);
    const [role] = useQueryParam('role', StringParam);

    useEffect(() => {
        scroll.scrollToBottom();
    }, []);

    return (
        <main className="chat-page">
            <ChatWrapper id={id} role={role} />
            <Button
                className="button-light"
                onClick={() => scroll.scrollToTop({ smooth: true, duration: 1 })}
                text="actions.scrollToTop"
            />
        </main>
    );
}
export default ChatPage;
