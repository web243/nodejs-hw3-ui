import { useQueryParam, StringParam } from 'use-query-params';
import React from 'react';
import LoadsTableWithFetch from '../../tables/loads_tables/loads_table';

function LoadsPage() {
    const [status] = useQueryParam('status', StringParam);
    return (
        <main>
            <LoadsTableWithFetch status={status ? status.toUpperCase() : null} />
        </main>
    );
}

export default LoadsPage;
