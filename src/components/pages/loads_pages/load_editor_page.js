import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import LoadEditorFormWithFormik from '../../forms/load_forms/load_editor_form';
import { getLoadByIdForUser } from '../../../services/loads_service';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';

function LoadEditor(props) {
    const { id } = props;
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [load, setLoad] = useState({ load: null, truck: null });

    useEffect(() => {
        getLoadByIdForUser(id)
            .then(
                (result) => {
                    setLoad(result.load);
                    setIsLoaded(true);
                },
                (e) => {
                    setError(e);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return <LoadEditorFormWithFormik load={load} />;
}

function LoadEditorPage() {
    const { id } = useParams();
    const { t } = useTranslation();
    return (
        <main>
            <h1>{t('CRUD.load.edit')}</h1>
            <LoadEditor id={id} />
        </main>
    );
}

export default LoadEditorPage;
