import React from 'react';
import { useTranslation } from 'react-i18next';
import LoadCreatorFormWithFormik from '../../forms/load_forms/load_creator_form';

function LoadCreatorPage() {
    const { t } = useTranslation();
    return (
        <main>
            <h1>{t('CRUD.load.add')}</h1>
            <LoadCreatorFormWithFormik />
        </main>
    );
}

export default LoadCreatorPage;
