import React, { useEffect, useState } from 'react';
import {
    useParams,
} from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { getShippingInfoForUser } from '../../../services/loads_service';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';
import LoadDossier from '../../tables/dossier_tables/load_dossier_table';
import TruckDossier from '../../tables/dossier_tables/truck_dossier_table';
import LogsTable from '../../tables/logs_table';
import NoDataFoundMessage from '../../blocks/messages/no_data_found_message';

function LoadsInfoPage(props) {
    const { load, truck } = props.info;
    const { t } = useTranslation();
    return load ? (
        <main>
            <h1>{load.name}</h1>
            <h2>{t('general.load')}</h2>
            <LoadDossier load={load} />
            <h2>{t('general.truck')}</h2>
            <TruckDossier truck={truck} />
            <h2>{t('general.message_plural')}</h2>
            <LogsTable logs={load.logs} />
        </main>
    ) : (
        <main>
            <NoDataFoundMessage />
        </main>
    );
}

function LoadsInfoPageWithFetch() {
    const { id } = useParams();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [info, setInfo] = useState({ load: null, truck: null });

    useEffect(() => {
        getShippingInfoForUser(id)
            .then(
                (result) => {
                    setIsLoaded(true);
                    setInfo(result);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="state.loading" />;
    }
    return <LoadsInfoPage info={info} />;
}

export default LoadsInfoPageWithFetch;
