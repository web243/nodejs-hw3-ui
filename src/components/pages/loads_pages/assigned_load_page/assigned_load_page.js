import { connect } from 'react-redux';
import React, { useEffect, useState } from 'react';
import mapStateToProps from '../../../../store/map_state_to_props';
import { getAssignedLoadForUser } from '../../../../services/loads_service';
import GlobalErrorBlock from '../../../blocks/global_error_block';
import TextBlock from '../../../blocks/message_block';
import LoadDossier from '../../../tables/dossier_tables/load_dossier_table';
import NextLoadStateButton from '../../../buttons/types/next_load_state_button';
import './assigned_load_page.css';
import { useTranslation } from 'react-i18next';
import NoDataFoundMessage from '../../../blocks/messages/no_data_found_message';

function AssignedLoadPage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [load, setLoad] = useState(null);
    const { t } = useTranslation();

    useEffect(() => {
        getAssignedLoadForUser()
            .then(
                (result) => {
                    setIsLoaded(true);
                    setLoad(result.load);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="state.loading" />;
    }
    return load ? (
        <main className="assigned-load-page">
            <h1>{t('info.load.assigned')}</h1>
            <LoadDossier load={load} />
            <NextLoadStateButton disabled={load.status !== 'ASSIGNED'
            || load.state === 'Arrived to delivery'}
            />
        </main>
    ) : <NoDataFoundMessage />;
}

export default connect(mapStateToProps)(AssignedLoadPage);
