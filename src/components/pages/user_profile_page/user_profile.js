import React from 'react';
import './user_profile.css';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import mapStateToProps from '../../../store/map_state_to_props';
import PasswordChangeFormWithFormik from '../../forms/password_change_form/password_change_form';
import DeleteProfileButton from '../../buttons/types/delete_profile_button';

function UserProfilePage(props) {
    const { user } = props;
    const { t } = useTranslation();
    return user ? (
        <main className="user-profile-container">
            <h1>{t('profile.info')}</h1>
            <p>{user.email}</p>
            <p>{user.role}</p>
            <h1>{t('profile.settings')}</h1>
            <h2>{t('profile.passwords.change')}</h2>
            <PasswordChangeFormWithFormik />
            <h2>{t('profile.delete')}</h2>
            <DeleteProfileButton />
        </main>
    ) : <Redirect to="/" />;
}

export default connect(mapStateToProps)(UserProfilePage);
