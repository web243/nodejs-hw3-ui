import { useParams } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getTruckByIdForUser } from '../../../services/trucks_service';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';
import TruckDossier from '../../tables/dossier_tables/truck_dossier_table';

function TruckInfoPage(props) {
    const { truck } = props;
    const { t } = useTranslation();
    return truck ? (
        <main>
            <h1>{t('general.truck')}</h1>
            <TruckDossier truck={truck} />
        </main>
    ) : (
        <main>
            <p className="message">No data found</p>
        </main>
    );
}

function TruckInfoPageWithFetch() {
    const { id } = useParams();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [truck, setTruck] = useState({});

    useEffect(() => {
        getTruckByIdForUser(id)
            .then(
                (result) => {
                    setIsLoaded(true);
                    setTruck(result);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return <TruckInfoPage truck={truck} />;
}

export default TruckInfoPageWithFetch;
