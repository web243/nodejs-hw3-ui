import React from 'react';
import { useTranslation } from 'react-i18next';
import TruckCreatorFormWithFormik from '../../forms/trucks_forms/truck_creator_form';

function TruckCreatorPage() {
    const { t } = useTranslation();
    return (
        <main>
            <h1>{t('CRUD.truck.add')}</h1>
            <TruckCreatorFormWithFormik />
        </main>
    );
}

export default TruckCreatorPage;
