import React from 'react';
import TrucksTableWithFetch from '../../tables/trucks_tables/trucks_table';

function TrucksPage() {
    return (
        <main>
            <TrucksTableWithFetch />
        </main>
    );
}

export default TrucksPage;
