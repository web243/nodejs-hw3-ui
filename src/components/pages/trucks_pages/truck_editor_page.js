import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { getTruckByIdForUser } from '../../../services/trucks_service';
import GlobalErrorBlock from '../../blocks/global_error_block';
import TextBlock from '../../blocks/message_block';
import TruckEditorFormWithFormik from '../../forms/trucks_forms/truck_editor_form';

function TruckEditor(props) {
    const { id } = props;
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [truck, setTruck] = useState({});

    useEffect(() => {
        getTruckByIdForUser(id)
            .then(
                (result) => {
                    setTruck(result);
                    setIsLoaded(true);
                },
                (e) => {
                    setError(e);
                    setIsLoaded(true);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return <TruckEditorFormWithFormik truck={truck} />;
}

function TruckEditorPage() {
    const { id } = useParams();
    const { t } = useTranslation();
    return (
        <main>
            <h1>{t('CRUD.truck.edit')}</h1>
            <TruckEditor id={id} />
        </main>
    );
}

export default TruckEditorPage;
