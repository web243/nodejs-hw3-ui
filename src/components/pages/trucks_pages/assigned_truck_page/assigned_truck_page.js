import React, { useEffect, useState } from 'react';
import GlobalErrorBlock from '../../../blocks/global_error_block';
import TextBlock from '../../../blocks/message_block';
import { getAssignedTruckForUser } from '../../../../services/trucks_service';
import TruckDossier from '../../../tables/dossier_tables/truck_dossier_table';
import './assigned_truck_page.css';
import NoDataFoundMessage from '../../../blocks/messages/no_data_found_message';
import { useTranslation } from 'react-i18next';

function AssignedTruckPage() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [truck, setTruck] = useState(null);
    const { t } = useTranslation();

    useEffect(() => {
        getAssignedTruckForUser()
            .then(
                (result) => {
                    setIsLoaded(true);
                    setTruck(result.truck);
                },
                (e) => {
                    setIsLoaded(true);
                    setError(e);
                },
            );
    }, []);

    if (error) {
        return <GlobalErrorBlock message={error.message} />;
    } if (!isLoaded) {
        return <TextBlock text="Loading..." />;
    }
    return truck ? (
        <main className="assigned-truck-page">
            <h1>{t('info.truck.assigned')}</h1>
            <TruckDossier truck={truck} />
        </main>
    ) : <NoDataFoundMessage />;
}

export default AssignedTruckPage;
