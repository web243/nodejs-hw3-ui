import React from 'react';
import { Redirect } from 'react-router-dom';
import './home_page.css';
import { connect } from 'react-redux';
import mapStateToProps from '../../../store/map_state_to_props';
import LoginPage from '../auth_pages/login_page';

function HomePage(props) {
    const { user } = props;
    let redirectTo = '/';
    if (user) {
        redirectTo = user.role === 'SHIPPER' ? '/loads?status=NEW' : '/trucks';
    }
    return user ? <Redirect to={redirectTo} /> : <LoginPage />;
}

export default connect(mapStateToProps)(HomePage);
