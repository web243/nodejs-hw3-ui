import React from 'react';
import { useTranslation } from 'react-i18next';

function ValidationErrorBlock(props) {
    const { message } = props;
    const { t } = useTranslation();
    return <p className="validation-error">{t(message)}</p>;
}

export default ValidationErrorBlock;
