import React from 'react';
import { useTranslation } from 'react-i18next';

function GlobalErrorBlock(props) {
    const { message } = props;
    const { t } = useTranslation();
    return <p className="short-description global-error">{t(message)}</p>;
}

export default GlobalErrorBlock;
