import React from 'react';
import { useTranslation } from 'react-i18next';

function TextBlock(props) {
    const { text } = props;
    const { t } = useTranslation();
    return <p className="message">{t(text)}</p>;
}

export default TextBlock;
