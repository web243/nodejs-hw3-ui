import React from 'react';
import { useTranslation } from 'react-i18next';
import capitalizeFirstLetter from './capitalize';

function NoDataFoundMessage() {
    const { t } = useTranslation();
    const message = t('messages.nothingFound',
        { what: t('general.data').toLowerCase() });
    return (
        <p className="message">
            {capitalizeFirstLetter(message)}
        </p>
    );
}

export default NoDataFoundMessage;
