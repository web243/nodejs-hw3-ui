import React from 'react';
import { useTranslation } from 'react-i18next';
import capitalizeFirstLetter from './capitalize';

function NoDataYetMessage(props) {
    const { t } = useTranslation();
    const { data } = props;
    const message = t('messages.nothingFoundYet',
        { what: t(`general.${data}_plural`).toLowerCase() });
    return (
        <p className="message">
            {capitalizeFirstLetter(message)}
        </p>
    );
}

export default NoDataYetMessage;
