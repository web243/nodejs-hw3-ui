import { useTranslation } from 'react-i18next';
import React from 'react';
import capitalizeFirstLetter from './capitalize';

function NoLoadsFoundMessage() {
    const { t } = useTranslation();
    const message = t('messages.nothingFound',
        { what: t('general.load_plural').toLowerCase() });
    return (
        <p className="message">
            {capitalizeFirstLetter(message)}
        </p>
    );
}

export default NoLoadsFoundMessage;
