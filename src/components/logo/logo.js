import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTruckMoving } from '@fortawesome/free-solid-svg-icons';
import './logo.css';
import { Link } from 'react-router-dom';

function Logo() {
    return (
        <Link to="/" className="logo">
            <FontAwesomeIcon icon={faTruckMoving} className="logo-icon" />
            <p className="logo-text">LOGO</p>
        </Link>
    );
}

export default Logo;
