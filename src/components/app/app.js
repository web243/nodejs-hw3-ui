import React, { useState, useEffect } from 'react';
import { Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Header from '../header/header';
import { getRoutes } from '../../routes';
import mapStateToProps from '../../store/map_state_to_props';
import Menu from '../menu/menu';
import './app.css';

function App(props) {
    const { user, language } = props;
    const [menuOpen, setMenuOpen] = useState(false);
    const { i18n } = useTranslation();

    useEffect(() => {
        i18n.changeLanguage(language);
    }, [language]);

    return (
        <React.Fragment>
            {user
               && (
                   <Header
                       openMenu={() => { setMenuOpen(true); }}
                       closeMenu={() => { setMenuOpen(false); }}
                   />
               )}
            <div className="wrapper main-container">
                {user && menuOpen && <Menu />}
                <Switch>
                    {getRoutes()}
                </Switch>
            </div>
        </React.Fragment>
    );
}

export default connect(mapStateToProps)(App);
