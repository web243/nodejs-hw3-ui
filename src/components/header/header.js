import React from 'react';
import './header.css';
import Logo from '../logo/logo';
import LogoutButton from '../buttons/types/auth_buttons/logout_button';
import OpenMenuButton from '../buttons/types/icons_buttons/open_menu_button';
import LanguagesButtons from '../buttons/types/languages_buttons/languages_buttons';

function Header(props) {
    const { openMenu, closeMenu } = props;
    return (
        <header className="wrapper header">
            <div className="header-icons">
                <OpenMenuButton openMenu={openMenu} closeMenu={closeMenu} />
                <Logo />
            </div>
            <div className="header-buttons-container">
                <LanguagesButtons />
                <LogoutButton />
            </div>
        </header>
    );
}

export default Header;
