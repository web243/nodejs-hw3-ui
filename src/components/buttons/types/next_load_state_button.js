import React from 'react';
import Button from '../button';
import { goToNextLoadState } from '../../../services/loads_service';

function NextLoadStateButton(props) {
    const { disabled } = props;
    return (
        <Button
            disabled={disabled}
            text="actions.iterateNextLoadState"
            onClick={() => {
                goToNextLoadState()
                    .then(() => {
                        document.location.reload();
                    });
            }}
        />
    );
}

export default NextLoadStateButton;
