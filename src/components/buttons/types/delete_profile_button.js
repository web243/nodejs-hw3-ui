import React from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Button from '../button';
import { deleteUserProfile } from '../../../services/users_service';
import { logOut } from '../../../store/reducers/user_reducer';

function DeleteProfileButton() {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    return (
        <Button
            text="profile.delete"
            onClick={() => {
                // eslint-disable-next-line no-restricted-globals
                const deleteAccount = confirm(
                    t('messages.confirmIfSure', { what: t('to permanently delete your profile') }),
                );
                if (deleteAccount) {
                    deleteUserProfile()
                        .then(({ message }) => {
                            if (message === 'Success') {
                                dispatch(logOut());
                            }
                        })
                        .catch((e) => {
                            alert(e);
                        });
                }
            }}
        />
    );
}

export default DeleteProfileButton;
