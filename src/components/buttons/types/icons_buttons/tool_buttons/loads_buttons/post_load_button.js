import { faCalendar } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import '../../icon_button.scss';
import { postLoadByIdForUser } from '../../../../../../services/loads_service';

function PostLoadButton(props) {
    const { id } = props;
    return (
        <button
            type="button"
            onClick={() => {
                postLoadByIdForUser(id)
                    .then(() => {
                        document.location.reload();
                    });
            }}
            className="icon-button"
        >
            <FontAwesomeIcon icon={faCalendar} className="icon" />
        </button>
    );
}

export default PostLoadButton;
