import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Link } from 'react-router-dom';
import '../../icon_button.scss';

function EditLoadButton(props) {
    const { id } = props;
    return (
        <Link to={`/loads/${id}/edit`} className="icon-button">
            <FontAwesomeIcon icon={faEdit} className="icon" />
        </Link>
    );
}

export default EditLoadButton;
