import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import '../../icon_button.scss';
import { deleteLoadByIdForUser } from '../../../../../../services/loads_service';

function DeleteLoadButton(props) {
    const { id } = props;
    return (
        <button
            onClick={() => {
                // eslint-disable-next-line no-restricted-globals
                const deleteLoad = confirm('Are you sure you want to permanently delete this load?');
                if (deleteLoad) {
                    deleteLoadByIdForUser(id)
                        .then(() => {
                            document.location.reload();
                        });
                }
            }}
            className="icon-button"
        >
            <FontAwesomeIcon icon={faTrashAlt} className="icon" />
        </button>
    );
}

export default DeleteLoadButton;
