import { faCalendarPlus } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import '../../icon_button.scss';
import { assignTruckForUser } from '../../../../../../services/trucks_service';

function AssignTruckButton(props) {
    const { id } = props;
    return (
        <button
            type="button"
            onClick={() => {
                assignTruckForUser(id)
                    .then(() => {
                        document.location.reload();
                    });
            }}
            className="icon-button"
        >
            <FontAwesomeIcon icon={faCalendarPlus} className="icon" />
        </button>
    );
}

export default AssignTruckButton;
