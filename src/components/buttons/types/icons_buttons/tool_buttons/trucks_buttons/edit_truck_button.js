import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import React from 'react';

function EditTruckButton(props) {
    const { id } = props;
    return (
        <Link to={`/trucks/${id}/edit`} className="icon-button">
            <FontAwesomeIcon icon={faEdit} className="icon" />
        </Link>
    );
}

export default EditTruckButton;
