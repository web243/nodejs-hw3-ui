import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import '../../icon_button.scss';
import { deleteTruckByIdForUser } from '../../../../../../services/trucks_service';

function DeleteTruckButton(props) {
    const { id } = props;
    return (
        <button
            onClick={() => {
                // eslint-disable-next-line no-restricted-globals
                const deleteTruck = confirm('Are you sure you want to permanently delete this truck?');
                if (deleteTruck) {
                    deleteTruckByIdForUser(id)
                        .then(() => {
                            document.location.reload();
                        });
                }
            }}
            className="icon-button"
        >
            <FontAwesomeIcon icon={faTrashAlt} className="icon" />
        </button>
    );
}

export default DeleteTruckButton;
