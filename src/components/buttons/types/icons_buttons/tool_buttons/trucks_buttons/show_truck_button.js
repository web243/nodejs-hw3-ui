import { faEye } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Link } from 'react-router-dom';
import '../../icon_button.scss';

function ShowTruckButton(props) {
    const { id } = props;
    return (
        <Link to={`/trucks/${id}`} className="icon-button">
            <FontAwesomeIcon icon={faEye} className="icon" />
        </Link>
    );
}

export default ShowTruckButton;
