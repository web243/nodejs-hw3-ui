import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faMinus } from '@fortawesome/free-solid-svg-icons';
import '../../button.css';

function OpenMenuButton(props) {
    const [menuOpened, setMenuOpened] = useState(false);
    return menuOpened
        ? (
            <button
                className="icon-button"
                type="button"
                onClick={() => {
                    setMenuOpened(false);
                    props.closeMenu();
                }}
            >
                <FontAwesomeIcon icon={faMinus} className="icon" />
            </button>
        )
        : (
            <button
                className="icon-button"
                type="button"
                onClick={() => {
                    setMenuOpened(true);
                    props.openMenu();
                }}
            >
                <FontAwesomeIcon icon={faBars} className="icon" />
            </button>
        );
}

export default OpenMenuButton;
