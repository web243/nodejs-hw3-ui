import React, { useState } from 'react';
import './languages_buttons.css';
import { useDispatch } from 'react-redux';
import { setLanguage } from '../../../../store/reducers/language_reducer';
import { getLanguageFromSessionStorage } from '../../../../store/language_session_storage';

function LanguageButton(props) {
    const { language, onClick, active } = props;
    return (
        <button
            className={`text-button ${active && 'text-button-active'}`}
            onClick={(e) => {
                onClick(e);
            }}
            value={language}
            type="button"
        >
            {language}
        </button>
    );
}

function LanguagesButtons() {
    const languages = ['EN', 'UA'];
    const language = getLanguageFromSessionStorage();
    const [active, setActive] = useState(language ? language.toUpperCase() : 'EN');
    const dispatch = useDispatch();
    return (
        <div className="languages-buttons-container">
            {languages.map((language) => (
                <LanguageButton
                    language={language}
                    onClick={(e) => {
                        const lang = e.currentTarget.value;
                        if (lang) {
                            setActive(lang);
                            dispatch(setLanguage(lang.toLowerCase()));
                        }
                    }}
                    active={language === active}
                />
            ))}
        </div>
    );
}

export default LanguagesButtons;
