import React from 'react';
import ButtonWithLink from '../../button_with_link';

function LoginButton() {
    return <ButtonWithLink path="/login" text="auth.logIn" />;
}

export default LoginButton;
