import React from 'react';
import { useDispatch } from 'react-redux';
import ButtonWithLink from '../../button_with_link';
import { logOut } from '../../../../store/reducers/user_reducer';

function LogoutButton() {
    const dispatch = useDispatch();
    return (
        <ButtonWithLink
            path="/"
            text="auth.logOut"
            onClick={() => {
                dispatch(logOut());
            }}
        />
    );
}

export default LogoutButton;
