import React from 'react';
import ButtonWithLink from '../../button_with_link';

function RegisterButton() {
    return <ButtonWithLink path="/register" text="auth.signUp" />;
}

export default RegisterButton;
