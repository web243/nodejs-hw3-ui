import { Link } from 'react-router-dom';
import React from 'react';
import Button from './button';

function ButtonWithLink(props) {
    const { path } = props;
    return (
        <Link to={path}>
            <Button {...props} />
        </Link>
    );
}

export default ButtonWithLink;
