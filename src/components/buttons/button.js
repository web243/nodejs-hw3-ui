import React from 'react';
import './button.css';
import { useTranslation } from 'react-i18next';

function Button(props) {
    const {
        className, type, text, onClick, disabled,
    } = props;
    const { t } = useTranslation();
    return (
        <button
            className={className}
            type={type || 'button'}
            onClick={onClick || (() => {})}
            disabled={disabled}
        >
            {t(text)}
        </button>
    );
}

export default Button;
